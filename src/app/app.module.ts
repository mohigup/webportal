import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {routing} from "./app.routing";
import {ListComponent} from "./list.component";
import {DetailComponent} from "./detail.component";
import {CommonModule, HashLocationStrategy, LocationStrategy} from "@angular/common";

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    DetailComponent,
    

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
  ],
  providers: [ {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
