import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { environment } from '../environments/environment';
import {FormBuilder, FormGroup} from '@angular/forms';
import { NgForm } from '@angular/forms';
import { Observable ,Subscription} from "rxjs";

import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";

import {Question} from './question';

@Component({
  //moduleId: __moduleName,
  selector: 'nyl-detail',
  templateUrl: 'detail.template.html'
  //styleUrls:  ['detail.component.css']
})
/** Class representing a DetailComponent. */
export class DetailComponent implements OnInit {
  question: Question ={};
  submitted:Boolean;
  id: string;
  http: Http;
  subscription: Subscription;
  agent: Object = {};
  observable$: Observable<{}>;
  constructor(http: Http,private  _activatedRoute: ActivatedRoute) {
    this.http = http;
    console.log("details");
  }
  ngOnInit () {

    this.subscription = this._activatedRoute.params.subscribe(
      (param: any) => {
        this.id = param['id'];
        console.log(this.id);
        console.log("id recieved");
      });
        this.observable$  =this.http
          .get(environment.server + "/public/detail/" + this.id)
          .map((response: Response) => {

            const data     = response.json ().obj;
            this.question =data;
            this.agent.category = data.CATEGORY;
            this.agent.question = data.QUESTION;
            this.agent.id = this.id;
            console.log(this.question);
            response.json();
          });



  }

  onSubmit() {
    console.log("inside submit");
    console.log(this.agent);
    const headers = new Headers ({
      'Content-Type': 'application/json'
    });
    const body    = JSON.stringify (this.agent);
    console.log("body");
    console.log(body);
    this.observable$  =this.http
      .post(environment.server + "/public/submit",body, {headers: headers})
      .map((response: Response) => {

       // const const data     = response.json ().obj;
       //  this.question =data;
       //  console.log(this.question);
       //  response.json();
        //delete this.agent;
        this.submitted =true;
      });
  }

  submitAnwer(){
    //this.QAForm.controls['comment'].value;
    // QAForm: FormGroup;
  }
}
