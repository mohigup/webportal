
export class Question {
  constructor (public id: string,

               public question: string,

               public answer: string,

               public status: string,

               public submittedby: string,
               public category:string,
               public uid: string

              ) {}
}
