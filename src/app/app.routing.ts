import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {ListComponent} from "./list.component";
import {DetailComponent} from "./detail.component";

const APP_ROUTES: Routes = [
  {path: '', component: ListComponent},
  { path: 'details/:id', component: DetailComponent },
  ];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
