import { Component, OnInit ,AfterViewInit} from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response } from "@angular/http";
import { environment } from '../environments/environment';

import { Observable } from "rxjs";

import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";

import {Question} from './question';

@Component({
  //moduleId: __moduleName,
 selector: 'nyl-list',
  templateUrl: 'list.template.html'
})
/** Class representing a ListComponent. */
export class ListComponent implements AfterViewInit {
  questions: Question[] = [];
  http: Http;

  observable$: Observable<{}>;
  constructor(http: Http) {
    this.http = http;
  }
  ngAfterViewInit (): any {
    console.log("asda");
    this.observable$  =this.http
      .get(environment.server + "/public/all")
      .map((response: Response) =>
      {
        const data     = response.json ().obj;
        let objs: any[] = [];
        for (let i = 0; i < data.length; i++) {
          let question = new Question(
            data[i].QID,
            data[i].QUESTION,
            data[i].ANSWER,
            data[i].QSTATUS,
            data[i].SUBMITTEDBY,
            data[i].CATEGORY,
            data[i].UID);
          objs.push(question);
        }
          this.questions = objs;
        console.log(this.questions);
          response.json();
      });

  }
}
